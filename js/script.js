// Create array
// Use an array literal: []

const array1 = ['eat', 'sleep'];

// Use the new keyword
const array2 = new Array('pray', 'play');

// empty array
const myList = [];

// array of numbers
const numArray = [2,3,4,5,6];

// array of mstrings
const stringArray = ['eat', 'work', 'play', 'pray'];

// array of mixed
const newData = ['work', 1, true];

const newData1 = [
	{'task': 'exercise'},
	[1,2,3],
	function hello(){
		console.log('hi I am array.')
	}
]
console.log(newData1);

/* Mini-Activity

	Create an array with 7 items all strings.
		-list seven of the places you want to visit someday.
		-Log the first item in the console.
		-Log all the items in the console.
*/

let arrayPlace = ['Maldives', 'Europe', 'Korea', 'USA', 'Japan', 'Dubai', 'Indonesia'];

console.log(arrayPlace[0]);
console.log(arrayPlace);

for(let i = 0; i < arrayPlace.length; i++){
	console.log(arrayPlace[i]);
}

//Array manipulation
// - push() 
//Adds element to an array at the end of the array

let dailyActivites = ['eat', 'work', 'pray', 'play'];
dailyActivites.push('excercise');
console.log(dailyActivites);

// - unshift 
// Adds element to an array at the beginning of the array

dailyActivites.unshift('sleep');
console.log(dailyActivites);

dailyActivites[2] = 'sing';
console.log(dailyActivites);

dailyActivites[6] = 'dance';
console.log(dailyActivites);

// Re-assign the values/items in an array:
arrayPlace[3] = 'Giza Sphinx';
console.log(arrayPlace);

/*Mini Activity
	Rea-assign the values for the first and last item in the array.
			-Re-assign it with your hometown and your highschool
			-Log the array in the console.
			-Log the first and last items in the console.
*/

arrayPlace[0] = 'Cebu';
arrayPlace[arrayPlace.length-1] = 'University of San Carlos';
console.log(arrayPlace[0]);
console.log(arrayPlace[arrayPlace.length-1]);

// Adding items in an array without using methods:
let array = [];
console.log(array[0]);
array[0] = 'Cloud Strife';
console.log(array[0]);
console.log(array [1]);
array [1] = 'Tifa Lockhart';
console.log(array[1]);
array[array.length-1] = 'Aerith Gainsborough';
console.log(array);
array[array.length] = 'Vincent Valentine';
console.log(array);

// Array Methods
	// Manipulate array with pre-determined JS functions.
	// Mutators - these arrays methods usually change the original array.

	let arrayX = ['Juan', 'Pedro', 'Jose', 'Andres'];
	// without method
	arrayX[arrayX.length] = 'Francisco';
	console.log(arrayX);

	//.push() - allows us to add an element at the end of the array.
	arrayX.push('Elmo');
	console.log(arrayX);

	// .unshift() - allows us to add an element at the beginning of the array.
	arrayX.unshift('Simon');
	console.log(arrayX);

	// .pop() - allows us to delete or remove the last item/element at the end of the array.
	arrayX.pop();
	console.log(arrayX);

	//.pop() is also able to return the item we removed.
	console.log(arrayX.pop());
	console.log(arrayX);

	let removedItem = arrayX.pop();
	console.log(arrayX);
	console.log(removedItem);

	// .shift() return item we removed
	let removedItemShift = arrayX.shift();
	console.log(arrayX);
	console.log(removedItemShift);

/*Mini-Activity

	Remove the first item of arrayX and log arrayX in the console.
	Delete the last item of arrayX and log arrayX in the console.
	add "george" at the start of the arrayX and log arrayX in the console.
	add "michael" at the end of the arrayX and log arrayX in the console.

	use array methods.
*/

	arrayX.pop();
	console.log(arrayX);
	arrayX.shift();
	console.log(arrayX);
	arrayX.push('Michael');
	console.log(arrayX);
	arrayX.unshift('George');
	console.log(arrayX);

	//.sort() - by default, will allow us to sort our items in ascending order.

	arrayX.sort();
	console.log(arrayX);

	let numArray1 = [3,2,1];
	numArray1.sort();
	console.log(numArray1);

	let numArray2 = [32, 400, 450, 100, 203, 4923, 240, 69];
	numArray2.sort((a,b)=>a-b);
	console.log(numArray2);
	// .sort() converts all items in to string items accordingly as if they are string.

	// ascending sort per number's value
	numArray2.sort(function(a,b){
		return a-b;
	})

	console.log(numArray2);

	let arrayStr = ['Marie', 'Zen', 'Jamie', 'Elaine'];
	arrayStr.sort();
	console.log(arrayStr);

	// .reverse() - reversed the order of the items.
	arrayStr.sort().reverse();
	console.log(arrayStr);

	//.splice() - allows us to remove and add elements from a given index.
	// Syntax: array.splice(startingIndex, numberofItemstobeDeleted, elementstoAdd)
	let lakersPlayers = ['Lebron', 'Davis', 'Westbrook', 'Kobe', 'Shaq'];

	lakersPlayers.splice(0, 0, 'Carusso');
	console.log(lakersPlayers);

	lakersPlayers.splice(0,1);
	console.log(lakersPlayers);

	lakersPlayers.splice(0, 3);
	console.log(lakersPlayers);


	lakersPlayers.splice(1,1);
	console.log(lakersPlayers);

	lakersPlayers.splice(1,0,'Gasol','Fisher');
	console.log(lakersPlayers);

	//Non-Mutators
		//Methods that will not change the original Array.

	// .slice() - allows us to get a portion of the original array and return a new array with the items selected from the original.
	// syntax: slice(startIndex, endIndex)

	let computerBrands = ['IBM', 'HP', 'Apple', 'MSI'];
	computerBrands.splice(2, 2, 'Compaq', 'Toshiba', 'Acer');
	console.log(computerBrands);

	let newBrands = computerBrands.slice(1,3);
	console.log(computerBrands);
	console.log(newBrands);

	let fonts = ['Times New Roman', 'Comic San MS', 'Impact', 'Monotype Corsiva', 'Arial', 'Arial Black'];

	let newFontSet = []
	newFontSet = fonts.slice(1,4);
	console.log(newFontSet);

	let newFontSet1 = fonts.slice(2);
	console.log(newFontSet1);

	/*
		Mini-Activity
		Given a set of data, use slice to copy the last two items in the array.
		Save the sliced portion of the array into a new variable:
			- microsoft (last two)
		use slice to copy the third and fourth item in the array.
		Save the sliced portion of the array into a new variable:
			- nintendo. (3rd 4th)
		log both new arrays in the console.
	*/


let videoGame = ['PS4', 'PS5', 'Switch', 'Xbox', 'Xbox1'];
	let microsoft = videoGame.slice(videoGame.length-2);
	let nintendo = videoGame.slice(2,4);
	console.log(microsoft);
	console.log(nintendo);

	// .toString() - convert the array into a single value.
	// syntax: array.toString();

	let sentence = ['I', 'like', 'Javascript', '.', 'It', 'is', 'fun', '.'];
	let sentenceString = sentence.toString();
	console.log(sentence);
	console.log(sentenceString);

	//.join() - converts the array into a single value as a string but separator can be specified.
	// syntax: array.join(separator)
	let sentence2 = ['My', 'favorite', 'fastfood', 'is', 'Army Navy'];
	let sentenceString2 = sentence2.join(' ');
	console.log(sentenceString2);

/*
	Mini-Activity

	Given a set of characters:
		-form the name, "Martin" as a single string.
		-form the name, "Miguel", as a single string.
	Use the methods we discussed so far

	save "Martin" and "Miguel" to variables:
		-name1 and name2
	Log both variables on the console.
*/
	let charArr = ["x",".","/","2","j","M","a","r","t","i","n","J","m","M","i","g","u","e","l","f","e","y"];

	let martinArray = charArr.slice(5,11);
	let name1 = martinArray.join('');
	console.log(name1);

	let miguelArray = charArr.slice(13,19);
	let name2 = miguelArray.join('');
	console.log(name2);

	// .concat() - it combines 2 or more arrays without affecting the original array.
	// syntax: array.concat(array1, array2);

	let tasksFriday = ['drink HTML', 'eat JavaScript'];
	let tasksSaturday = ['inhale CSS', 'breath BootStrap'];
	let tasksSunday = ['Get Git', 'Be Node'];

	let weekendTasks = tasksFriday.concat(tasksSaturday,tasksSunday);
	console.log(weekendTasks);

	// Accessors
		// Methods that allow us to access our array.
		
		// indexOf - finds the index of the given element/item when it is first found from the left.

	let batch131 = [
		'Paolo',
		'Jamir',
		'Jed',
		'Ronel',
		'Rommel',
		'Jayson'
	];

	console.log(batch131.indexOf('Jed'));
	console.log(batch131.indexOf('Rommel'));

		// lastIndexOf() - finds the index of the given element/item when it is first found from the right.
	console.log(batch131.lastIndexOf('Jamir'));

/*
	Mini-Activity
	Given a set of brands with some entries repeated:
		Create a function which can display the index of the brand that was input the first time it was found in the array.

		Create a function which can display the index of the brand that was input the last time it was found in the array.
*/

let carBrands = [
	'BMW',
	'Dodge',
	'Maserati',
	'Porsche',
	'Chevrolet',
	'Ferrari',
	'GMC',
	'Porshe',
	'Mitsubishi',
	'Toyota',
	'Volkswagen',
	'BMW'
]

function first(first){

	console.log(carBrands.indexOf(first));
}

function last(last){

	console.log(carBrands.lastIndexOf(last));
}

first('BMW');
last('BMW');

// Iterator Methods
	// These methods iterate over the items in an array much like loop
	// However, with our iterator methods there are also that allows to not only iterate over items but also additional instruction.
	
let avengers = [
	'Hulk',
	'Black Widow',
	'Hawkeye',
	'Spider-man',
	'Iron Man',
	'Captain America'
];

// forEach()
	// similar to for loop but is used on arrays. It will allow us to iterate over each time in an array and even add instruction per iteration.

// Anonymous function within forEach will receive each and every item in an array.

avengers.forEach(function(avengers){
	console.log(avengers);
});

console.log(avengers);

let marvelHeroes = [
	'Moon Knight',
	'Jessica Jones',
	'Deadpool',
	'Cyclops'
];

marvelHeroes.forEach(function (hero){
	// iterate over all the items in Marvel Heroes and let them join the avengers.
	if(hero !== 'Cyclops' && hero !== 'Deadpool'){
		// add an if else wherein cyclops and deadpool is not allowed to join.
		avengers.push(hero);
	}
	console.log(avengers);
});

// map() - similar to forEach however it returns new array.

let number = [24, 50, 30, 10, 5];

let mappedNumbers = number.map(function(number){
	console.log(number);
	return number*5;
});

console.log(mappedNumbers);

// every() - iterates over all the items and checks if all the elements passes a given condition.

let allMemberAge = [25, 30, 15, 20, 26];

let checkAllAdult = allMemberAge.every(function(age){
	console.log(age);
	return age >= 18
});

console.log(checkAllAdult);

// some() - iterates over all the items and checks if at least one of the items in the array passes the condition.

let examScores = [75, 80, 74, 71];

let checkForPassing = examScores.some(function(score){
	console.log(score);
	return score >= 80;
});

console.log(checkForPassing);

// filter() - creates a new array that contains elements which passed a given condition.

let numbersArr2 = [500, 12, 120, 60, 6, 30];
let divisibleBy5 = numbersArr2.filter(function(number){
	return number%5 === 0;
});
console.log(divisibleBy5);


// .find() - iterates over all items in our array but only returns the item that will satisfy the given condition.

let registeredUsernames = ['pedro101', 'mikeyTheKing2000', 'superPhoenix', 'sheWhoCode'];

let foundUser = registeredUsernames.find(function(username){
	console.log(username);
	return username === 'sheWhoCode';
});
console.log(foundUser);

// .includes() - returns a boolean true if it finds a matching item in the array. (this is case sensitive)

let registeredEmail = [
	'johnnyPhoenix1991@gmail.com',
	'michaelKing@gmail.com',
	'pedro_himself@yahoo.com',
	'sheJonesSmith@gmail.com'
];

let doesEmailExist = registeredEmail.includes('michaelKing@gmail.com');
console.log(doesEmailExist);

/*
	Mini-Activity
	Create 2 functions
		First function is able to find specified username input in our registeredUsernames array
		Display the result in the console.

		Second function is able to find specified email already exists in the registeredEmail.
			-If there is an email found, show an alert:
				"Email Already Exist"
			-If there is no email found, show an alert:
				"Email is available, proceed to registration."
*/

function findUser(username){
	let foundUser = registeredUsernames.find(function(username){
		console.log(username);
		return username;
	});
}

findUser('superPhoenix')

function checkEmail(email){
	let existEmail = registeredEmail.includes(email);
	if (existEmail == true){
		alert("Email Already Exist")
	} else alert("Email is available, proceed to registration")
};
checkEmail('johndoe@gmail.com');
